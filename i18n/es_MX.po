# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* woocommerce_odoo_connector
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 14.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-26 03:30+0000\n"
"PO-Revision-Date: 2021-09-28 10:57-0500\n"
"Last-Translator: Isaac Meza <imeza@gaeasistemas.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: \n"
"Language: es_MX\n"
"X-Generator: Poedit 3.0\n"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_categories__update_order_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_orders__update_order_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_partners__update_order_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_products__update_order_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_shipping__update_order_date
msgid ""
"\n"
"\tA date used for selecting orders that were last updated after (or at) a specified time.\n"
"\t An update is defined as any change in order status,includes updates made by the seller.\n"
msgstr ""
"\n"
"\tUna fecha que se utiliza para seleccionar los pedidos que se actualizaron por última vez después de (o en) un momento específico.\n"
"\t Una actualización se define como cualquier cambio en el estado del pedido, incluye las actualizaciones realizadas por el vendedor.\n"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_categories__import_order_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_orders__import_order_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_partners__import_order_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_products__import_order_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_shipping__import_order_date
msgid "A date used for selecting orders created after (or at) a specified time."
msgstr "Una fecha utilizada para seleccionar pedidos creados después (o en) un momento específico."

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__api_record_limit
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__api_record_limit
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__api_record_limit
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__api_record_limit
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__api_record_limit
msgid "API Record Limit"
msgstr "Límite de registro de API"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_attributes__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_attributes_value__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_categories__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_mws_orders__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_mws_products__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_operation__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_orders__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_partners__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_products__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_templates__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_categories__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_orders__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_partners__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_products__woocommerce_filter_type__all
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_shipping__woocommerce_filter_type__all
msgid "All"
msgstr "Todos"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_attributes__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_attributes_value__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_categories__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_mws_orders__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_mws_products__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_operation__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_orders__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_partners__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_products__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_templates__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_categories__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_orders__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_partners__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_products__woocommerce_filter_type__by_date
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_shipping__woocommerce_filter_type__by_date
msgid "By Date"
msgstr "Por Fecha"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_attributes__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_attributes_value__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_categories__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_mws_orders__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_mws_products__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_operation__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_orders__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_partners__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_products__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_templates__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_categories__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_orders__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_partners__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_products__woocommerce_filter_type__by_id
#: model:ir.model.fields.selection,name:woocommerce_odoo_connector.selection__import_woocommerce_shipping__woocommerce_filter_type__by_id
msgid "By Id"
msgstr "Por ID"

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/models/woc_config.py:0
#, python-format
msgid "Can't update product stock , "
msgstr "No se puede actualizar el stock del producto, "

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__category_ids
msgid "Categories ID(s)"
msgstr "ID(s) de Categorías"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__channel
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__channel
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__channel
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__channel
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__channel
msgid "Channel"
msgstr "Canal"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__channel_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__channel_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__channel_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__channel_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__channel_id
msgid "Channel ID"
msgstr "ID del canal"

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/models/woc_config.py:0
#, python-format
msgid "Connection Error: %s, %s"
msgstr "Error de conexión: %s, %s"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_multi_channel_sale__woocommerce_consumer_key
msgid "Consumer Key"
msgstr "Llave del consumidor"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__create_uid
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__create_uid
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__create_uid
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__create_uid
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__create_uid
msgid "Created by"
msgstr "Creado por"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__create_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__create_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__create_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__create_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__create_date
msgid "Created on"
msgstr "Creado en"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__import_customer_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__import_customer_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__import_customer_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__import_customer_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__import_customer_date
msgid "Customer Created After"
msgstr "Cliente creado después"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__update_customer_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__update_customer_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__update_customer_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__update_customer_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__update_customer_date
msgid "Customer Updated After"
msgstr "Cliente actualizado después"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_export_categories__display_name
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_export_products__display_name
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_export_templates__display_name
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_operation__display_name
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__display_name
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__display_name
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__display_name
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__display_name
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__display_name
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_multi_channel_sale__display_name
msgid "Display Name"
msgstr "Nombre desplegado"

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/wizard/import_attributes.py:0
#, python-format
msgid "Error : "
msgstr "Error:"

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/wizard/export_product.py:0
#, python-format
msgid "Error in Creating Product Template in Woocommerce."
msgstr "Error al crear una plantilla de producto en Woocommerce."

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/wizard/export_update_product.py:0
#, python-format
msgid "Error in Updating Product Template in Woocommerce."
msgstr "Error al actualizar la plantilla de producto en Woocommerce."

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/wizard/export_product.py:0
#, python-format
msgid "Error in creating Product Variant with id (%s)"
msgstr "Error al crear la variante de producto con id (%s)"

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/wizard/export_update_product.py:0
#, python-format
msgid "Error in updating Product Variant with id (%s)"
msgstr "Error al actualizar la variante de producto con id (%s)"

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/models/woc_config.py:0
#, python-format
msgid "Error:"
msgstr "Error:"

#. module: woocommerce_odoo_connector
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.inherit_export_odoo_categories_form
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.inherit_export_odoo_templates_form
msgid "Export"
msgstr "Exportar"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_export_categories
msgid "Export Category"
msgstr "Exportar Categoría"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_export_products
msgid "Export Partner"
msgstr "Exportar Empresa"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_export_templates
msgid "Export Template"
msgstr "Exportar Plantilla"

#. module: woocommerce_odoo_connector
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.inherit_import_wizard_form
msgid "Filter Type"
msgstr "Filtrar por tipo"

#. module: woocommerce_odoo_connector
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.inherit_import_wizard_form
msgid "From"
msgstr "Desde"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__from_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__from_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__from_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__from_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__from_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_categories__from_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_orders__from_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_partners__from_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_products__from_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_shipping__from_date
msgid "From Date"
msgstr "Partir de la fecha"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_export_categories__id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_export_products__id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_export_templates__id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_operation__id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_multi_channel_sale__id
msgid "ID"
msgstr "ID"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__image
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__image
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__image
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__image
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__image
msgid "Image"
msgstr "Imagen"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_import_operation
msgid "Import Operation"
msgstr "Importar operaciones"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_import_woocommerce_categories
msgid "Import Woocommerce Categories"
msgstr "Importar categorías de Woocommerce"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_import_woocommerce_orders
msgid "Import Woocommerce Orders"
msgstr "Importar pedidos de Woocommerce"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_import_woocommerce_partners
msgid "Import Woocommerce Partners"
msgstr "Importar socios de Woocommerce"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_import_woocommerce_products
msgid "Import Woocommerce Products"
msgstr "Importar productos de Woocommerce"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_import_woocommerce_shipping
msgid "Import Woocommerce Shipping"
msgstr "Importar envío de Woocommerce"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_export_categories____last_update
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_export_products____last_update
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_export_templates____last_update
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_operation____last_update
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories____last_update
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders____last_update
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners____last_update
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products____last_update
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping____last_update
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_multi_channel_sale____last_update
msgid "Last Modified on"
msgstr "Última modificación el"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__write_uid
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__write_uid
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__write_uid
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__write_uid
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__write_uid
msgid "Last Updated by"
msgstr "Ultima actualizacion por"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__write_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__write_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__write_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__write_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__write_date
msgid "Last Updated on"
msgstr "Última actualización en"

#. module: woocommerce_odoo_connector
#: model:ir.model,name:woocommerce_odoo_connector.model_multi_channel_sale
msgid "Multi Channel Sale"
msgstr "Ventas multi canal"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__object
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__object
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__object
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__object
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__object
msgid "Object"
msgstr "Objeto"

#. module: woocommerce_odoo_connector
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.inherit_import_wizard_form
msgid "Object Id"
msgstr "ID del objeto"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__operation
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__operation
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__operation
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__operation
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__operation
msgid "Operation"
msgstr "Operación"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__import_order_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__import_order_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__import_order_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__import_order_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__import_order_date
msgid "Order Created After"
msgstr "Orden creada después"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__order_ids
msgid "Order ID(s)"
msgstr "ID(s) de la orden(es)"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__update_order_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__update_order_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__update_order_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__update_order_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__update_order_date
msgid "Order Updated After"
msgstr "Orden actualizada posteriormente"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__parent_categ_id
msgid "Parent Category"
msgstr "Categoría padre"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__partner_ids
msgid "Partners ID(s)"
msgstr "ID(s) de empresa(s)"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__import_product_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__import_product_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__import_product_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__import_product_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__import_product_date
msgid "Product Created After"
msgstr "Producto creado después"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__product_tmpl_ids
msgid "Product Template ID(s)"
msgstr "ID(s) de plantilla(s) de producto(s)"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__update_product_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__update_product_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__update_product_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__update_product_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__update_product_date
msgid "Product Updated After"
msgstr "Producto actualizado posteriormente"

#. module: woocommerce_odoo_connector
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.inherited_multi_channel_sale_woocommerce
msgid "Secret Key"
msgstr "Secret Key"

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/wizard/export_product.py:0
#, python-format
msgid "Simple Product Creation Failed"
msgstr "Falló la creación de un producto simple"

#. module: woocommerce_odoo_connector
#: code:addons/woocommerce_odoo_connector/wizard/export_update_product.py:0
#, python-format
msgid "Simple Product Updation Failed"
msgstr "Error en la actualización simple del producto"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__source
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__source
msgid "Source"
msgstr "Origen"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__to_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__to_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__to_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__to_date
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__to_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_categories__to_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_orders__to_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_partners__to_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_products__to_date
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_import_woocommerce_shipping__to_date
msgid "To Date"
msgstr "A la fecha"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_multi_channel_sale__woocommerce_url
msgid "URI"
msgstr "URI"

#. module: woocommerce_odoo_connector
#: model:crm.team,name:woocommerce_odoo_connector.sales_team_woocommerce
msgid "WooCommerce Sales"
msgstr "WooCommerce Ventas"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_attributes__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_attributes_value__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_categories__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_mws_orders__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_mws_products__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_operation__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_orders__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_partners__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_products__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_templates__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__woocommerce_filter_type
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__woocommerce_filter_type
msgid "Woocommerce Filter Type"
msgstr "Woocommerce Tipo de filtro"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_attributes__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_attributes_value__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_categories__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_mws_orders__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_mws_products__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_operation__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_orders__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_partners__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_products__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_templates__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__woocommerce_import_date_from
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__woocommerce_import_date_from
msgid "Woocommerce Import Date From"
msgstr "Woocommerce Importar desde la fecha"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_attributes__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_attributes_value__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_categories__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_mws_orders__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_mws_products__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_operation__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_orders__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_partners__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_products__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_templates__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_categories__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_orders__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_partners__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_products__woocommerce_object_id
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_import_woocommerce_shipping__woocommerce_object_id
msgid "Woocommerce Object"
msgstr "Woocommerce Objeto"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,field_description:woocommerce_odoo_connector.field_multi_channel_sale__woocommerce_secret_key
msgid "Woocommerce Secret Key"
msgstr "Woocommerce Llave secreta"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_multi_channel_sale__woocommerce_consumer_key
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.inherited_multi_channel_sale_woocommerce
msgid "eg. ck_ccac94fc4362ba12a2045086ea9db285e8f02ac9"
msgstr "ej. ck_ccac94fc4362ba12a2045086ea9db285e8f02ac9"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_multi_channel_sale__woocommerce_secret_key
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.inherited_multi_channel_sale_woocommerce
msgid "eg. cs_a4c0092684bf08cf7a83606b44c82a6e0d8a4cae"
msgstr "ej. cs_a4c0092684bf08cf7a83606b44c82a6e0d8a4cae"

#. module: woocommerce_odoo_connector
#: model:ir.model.fields,help:woocommerce_odoo_connector.field_multi_channel_sale__woocommerce_url
msgid "eg. http://xyz.com"
msgstr "ej. http://xyz.com"

#. module: woocommerce_odoo_connector
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.inherited_multi_channel_sale_woocommerce
msgid "http://woocommerce-store-url"
msgstr "http://woocommerce-store-url"

#. module: woocommerce_odoo_connector
#: model_terms:ir.ui.view,arch_db:woocommerce_odoo_connector.multi_channel_view_kanban
msgid "woocommerce icon"
msgstr "woocommerce icon"
