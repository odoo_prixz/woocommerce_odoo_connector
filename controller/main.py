# -*- coding: utf-8 -*-
import json
import logging
from datetime import datetime, timedelta
from odoo import http
from odoo.http import request
import odoo

_logger = logging.getLogger(__name__)


class WebHookController(http.Controller):

    def search_order_and_feed(self, order):
        order_feed = request.env["order.feed"].sudo().search([
            ("name", "=", order)
        ])
        order_id = request.env["sale.order"].sudo().search([
            ("name", "=", order)
        ])
        return order_feed, order_id

    @http.route('/web/webhooks/woo/update/', type='json', auth='public', save_session=False)
    def webhook_woo_order_update(self, **kwargs):
        json_request = request.jsonrequest
        order = json_request.get("id")
        status = json_request.get("status")
        meta_data = json_request.get("meta_data")
        order_feed, order_id = self.search_order_and_feed(order)
        if json_request.get("date_paid") and status != "completed":
            self.create_order_cron(order, status, body=json_request)
        try:
            if order_feed and order_id:
                order_feed.message += f'<br/>Estado actualizado. {status}'
                order_feed.order_state = status
                order_id.order_state_woo = status
                """for line in meta_data:
                    if line["key"] == "_alg_checkout_files_upload_1":
                        if not order_feed.woo_receta:
                            order_feed.woo_receta = "https://s3.amazonaws.com/docs.prixz.com/" + line[
                                "value"] if not "/" in line["value"] else line["value"]
                            order_id.woo_receta = order_feed.woo_receta
                        break
                date_paid = (datetime.fromisoformat(
                    json_request.get("date_paid")) + timedelta(hours=5)) if json_request.get("date_paid") else None
                order_feed.woo_date_paid = date_paid
                order_id.woo_date_paid = order_feed.woo_date_paid"""
                return json.dumps({"success": "updated"})
            return json.dumps({"success": "sin orden"})
        except Exception as e:
            return json.dumps({"error": str(e)})

    @http.route('/web/webhooks/woo2/update/', type='json', auth='public', save_session=False)
    def webhook_woo2_order_update(self, **kwargs):
        json_request = request.jsonrequest
        order = json_request.get("id")
        status = json_request.get("status")
        meta_data = json_request.get("meta_data")
        order_feed, order_id = self.search_order_and_feed(order)
        if json_request.get("date_paid") and status != "completed":
            self.create_order_cron(order, status, "woocommerce2", body=json_request)
            return json.dumps({"success": "sin orden"})
        return json.dumps({"error": str("Error")})


    @http.route('/web/webhooks/woo3/update/', type='json', auth='public', save_session=False)
    def webhook_woo3_order_update(self, **kwargs):
        json_request = request.jsonrequest
        order = json_request.get("id")
        status = json_request.get("status")
        meta_data = json_request.get("meta_data")
        order_feed, order_id = self.search_order_and_feed(order)
        if json_request.get("date_paid") and status != "completed":
            self.create_order_cron(order, status, "woocommerce3", body=json_request)
            return json.dumps({"success": "sin orden"})
        return json.dumps({"error": str("Error")})

    @http.route('/web/webhooks/woo4/update/', type='json', auth='public', save_session=False)
    def webhook_woo4_order_update(self, **kwargs):
        json_request = request.jsonrequest
        order = json_request.get("id")
        status = json_request.get("status")
        meta_data = json_request.get("meta_data")
        order_feed, order_id = self.search_order_and_feed(order)
        if json_request.get("date_paid") and status != "completed":
            self.create_order_cron(order, status, "woocommerce4", body=json_request)
            return json.dumps({"success": "sin orden"})
        return json.dumps({"error": str("Error")})

    def create_order_cron(self, order, status, tipo=None, body=None):
        order_to_update = request.env(user=odoo.SUPERUSER_ID)['orders.to.update']
        if not tipo:
            orders = order_to_update.search([
                ("name", "=", order),
                ("estado", "=", "to_update")
            ])
            if not orders:
                vals = {
                    "name": order,
                    "estado_woo": status,
                    "estado": "to_update",
                    "body": body,
                }
                order_to_update.create(vals)
                request.cr.commit()
        else:
            orders = order_to_update.search([
                ("name", "=", order),
                ("estado", "=", "to_update"),
                ("tipo", "=", tipo)
            ])
            if not orders:
                vals = {
                    "name": order,
                    "estado_woo": status,
                    "estado": "to_update",
                    "tipo": tipo,
                    "body": body,
                }
                order_to_update.create(vals)
                request.cr.commit()
