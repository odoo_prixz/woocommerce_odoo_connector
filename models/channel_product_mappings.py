from odoo import models, _
from woocommerce import API
import logging
import time
import threading


_logger = logging.getLogger(__name__)


class ChannelProductMappings(models.Model):
    _inherit = 'channel.product.mappings'

    def _prepare_datas_update_stock(self, products_maps, cantidad_lote):
        products = []
        all_products = []
        contador = 0
        for product_map in products_maps:
            data = {
                "id": product_map.store_product_id,
                "stock_quantity": int(0),
            }
            products.append(data)
            contador = contador + 1
            if contador == cantidad_lote:
                contador = 0
                all_products.append(products)
                products = []
        if contador < cantidad_lote:
            all_products.append(products)
        return all_products

    def proceso_enviar_por_lote(self, connection, url, all_products):
        _logger.warning("********Inicio proceso %s *************", str(threading.current_thread().getName()))
        contador = 0
        for lines in all_products:
            data = {
                "updated": lines,
            }
            inicio = time.time()
            request_post = connection.post(
                url,
                data
            )
            fin = time.time()
            tiempo_final = fin - inicio
            _logger.warning("********Proceso %s tiempo : %s linea: %s", str(threading.current_thread().getName()), str(tiempo_final), str(contador))
            contador = contador + 1
            if not request_post.ok:
                _logger.warning("lote no se actualizo correcto: %s \n %s", str(lines), str(request_post.request.body))
            else:
                _logger.warning("lote correcto: %s \n %s", str(lines), str(request_post.text))

    def proceso_enviar_producto(self, connection, url, all_products):
        _logger.warning("********Inicio proceso %s *************", str(threading.current_thread().getName()))
        contador = 0
        inicio = time.time()
        for lines in all_products:
            for line in lines:
                data = {
                    "stock_quantity": int(0),
                }
                path = url + line['id']
                request_post = connection.post(path, data)
                contador = contador + 1
                if not request_post.ok:
                    _logger.warning("lote no se actualizo correcto: %s \n %s", str(lines), str(request_post.request.body))
                else:
                    _logger.warning("lote correcto: %s \n %s", str(lines), str(request_post.request.body))
        fin = time.time()
        tiempo_final = fin - inicio
        _logger.warning("********Proceso %s tiempo : %s linea: %s", str(threading.current_thread().getName()),
                    str(tiempo_final), str(contador))

    def get_all_products_without_san_bernabe(self):
        product_mappings = self.search([])
        _logger.warning("********Total de productos en feed: %s", str(len(product_mappings)))
        products = []
        san_bernabe = {}
        #obtengo los productos que estan de san bernabe
        for mapping in product_mappings:
            stock_id = self.env["stock.quant"].search([
                ("location_id.name", "=", "Stock"),
                ("location_id.location_id.name", "=", "CRUMS"),
                ("product_id", "=", mapping.product_name.id)
            ], order='id desc')
            if stock_id:
                san_bernabe[str(mapping.store_product_id)] = stock_id.available_quantity
        for mapping in product_mappings:
            stock_id = self.env["stock.quant"].search([
                ("location_id.name", "=", "Stock"),
                ("location_id.location_id.name", "!=", "CRUMS"),
                ("product_id", "=", mapping.product_name.id)
            ], order='id desc')
            #si hay en otro crum pero verifico que tampoco este en mi diccionario san bernabe
            if stock_id and mapping.store_product_id not in san_bernabe:
                products.append(mapping)
            #que no tenga ni san bernabe ni en otra parte tambien se agrega
            elif not stock_id and mapping.store_product_id not in san_bernabe:
                products.append(mapping)
        _logger.warning("********Total de productos en feed: %s que no son de san bernabe************", str(len(products)))
        return products

    def _cron_update_stock_without_bernabe_woo(self):
        numero_procesos = 5
        cantidad_lote = 20
        url = "products/"
        products_maps = self.get_all_products_without_san_bernabe()
        connection = self._get_woocommerce_connection()
        all_products = self._prepare_datas_update_stock(products_maps, cantidad_lote)
        if connection:
            tam_lista = len(all_products)
            rango = 1 if int(tam_lista/numero_procesos) == 0 else int(tam_lista/numero_procesos)
            inicio = 0
            fin = rango
            while fin <= tam_lista:
                hilo = threading.Thread(target=self.proceso_enviar_producto, args=(connection, url, all_products[inicio:fin]))
                hilo.start()
                inicio = fin
                fin = fin + rango
            hilo.join()
            _logger.warning("********FIN************")

    def _get_woocommerce_connection(self):
        channel_id = self.env['multi.channel.sale'].sudo().search([
            ("channel", "=", "woocommerce"),
            ("state", "=", "validate")
        ], limit=1)
        if channel_id:
            req = API(
                url=channel_id.woocommerce_url,
                consumer_key=channel_id.woocommerce_consumer_key,
                consumer_secret=channel_id.woocommerce_secret_key,
                #wp_api=True,
                version="wc/v3",
                #query_string_auth=True,
                timeout=999,
            )
            return req
        else:
            _logger.info("No connection channel Woo available")
            return False
