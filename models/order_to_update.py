from odoo import fields, models
from ...odoo_multi_channel_sale.ApiTransaction import Transaction
import time
import logging
_logger = logging.getLogger(__name__)


class OrdersToUpdate(models.Model):
    _name = "orders.to.update"
    _description = "Muestra las ordenes que se actualizaran en el cron"

    name = fields.Char(string="Pedido en woo", )
    estado_woo = fields.Char(string="Estado woo", )
    estado = fields.Selection(selection=[
        ('to_update', 'A actualizar'),
        ('updated', 'Actualizado'),
        ('error', 'Error')], string="Estado"
    )
    tipo = fields.Char(string="Tipo", )
    body = fields.Char(string="Body", )
    order_id = fields.Many2one("sale.order", string="Order")
    incidencia = fields.Char(string="incidencia")

    def _cron_update_feeds(self, tipo=None):
        _logger.info("****************Actualiza cron import/order*****************")
        channel_id = self.env['multi.channel.sale'].search([
            ("channel", "=", "woocommerce"),
            ("state", "=", "validate"),
            ("name", "=", "WooCommerce Prixz")
        ], limit=1)
        orders = self.import_orders_from_cron(channel_id, 4)
        if not orders:
            _logger.info("****************Actualiza cron import/order 2*****************")
            time.sleep(5)
            orders = self.import_orders_from_cron(channel_id, 4)

    def import_orders_from_cron(self, channel_id, limit=4):
        orders = self.search([
            ("estado", "=", "to_update"),
            ("tipo", "=", None)
        ], order='create_date ASC', limit=limit)
        for order in orders:
            kwargs = {
                'object': 'sale.order',
                'woocommerce_object_id': order.name,
                'woocommerce_import_date_from': False,
                'page': 1,
                'from_cron': True
            }
            try:
                Transaction(channel=channel_id).import_data(**kwargs)
            except Exception as e:
                _logger.info(f'****ERROR CRON ORDER {order.name} {e}')
                order.estado = 'error'
            order_id = self.env["sale.order"].search([("name", "=", order.name)])
            if order_id:
                order.estado = "updated"
                order.order_id = order_id.id
            self.env.cr.commit()
        return orders

    def _cron_update_feeds_instancias(self, tipo=None):
        _logger.info("****************Actualiza cron import/order instancias*****************")
        orders = self.search([
            ("estado", "=", "to_update"),
            ("tipo", "=", tipo)
        ], order='create_date ASC',)
        channel_id = self.env['multi.channel.sale'].search([
            ("channel", "=", "woocommerce"),
            ("state", "=", "validate"),
            ("name", "=", tipo)
        ], limit=1)

        for order in orders:
            kwargs = {
                'object': 'sale.order',
                'woocommerce_object_id': order.name,
                'woocommerce_import_date_from': False,
                'page': 1,
                'from_cron': True
            }
            try:
                Transaction(channel=channel_id).import_data(**kwargs)
            except Exception as e:
                _logger.info(f'****ERROR CRON ORDER {order.name} {e}')
                order.estado = 'error'
            order_id = self.env["sale.order"].search([("name", "=", order.name)])
            if order_id:
                order.estado = "updated"
                order.order_id = order_id.id
            else:
                order.estado = "to_update"
            self.env.cr.commit()

    def checar_ordenes_atoradas(self, max_ordenes_cola=5,):
        orders = self.search([
            ("estado", "=", "to_update"),
        ], order='create_date ASC')
        order_groups = self.group_orders_by_type(orders)
        for key, value in order_groups.items():
            if len(value) >= max_ordenes_cola:
                self.send_email_max_ordenes_cola(tipo=key, ordenes=value)

    def send_email_max_ordenes_cola(self, tipo=None, ordenes=0):
        channels = self.env['mail.channel'].search([
            ("name", "=", "Ordenes atoradas")
        ])
        if channels:
            for partner in channels.channel_last_seen_partner_ids:
                mail_values = {
                    'email_from': "odoo@prixz.com",
                    'reply_to': '',
                    'email_to': partner.partner_id.email or '',
                    'subject': 'Límite de ordenes en cron',
                    'body_html': f'Hola {partner.partner_id.name},\n'
                                 f' Tenemos {len(ordenes)} ordenes atoradas en el cron de la instancia {tipo}\n'
                                 f'Las ordenes son: {ordenes}',
                    'notification': True,
                    'auto_delete': False,
                }
                mail = self.env['mail.mail'].sudo().create(mail_values)
                mail.send()

    def group_orders_by_type(self, orders):
        orders_group = {}
        for order in orders:
            if order.tipo:
              tipo = order.tipo
            else:
                tipo = "prixz"
            if orders_group.get(tipo):
                orders_group[tipo].append(order.name)
            else:
                orders_group[tipo] = []
                orders_group[tipo].append(order.name)
        return orders_group
