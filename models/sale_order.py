from odoo import api, fields, models
import json
from datetime import datetime, timedelta
import requests
import logging

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    woo_date_paid = fields.Datetime(
        string="Fecha de pago en woo",
        help="Fecha de pago realizado en woo solo aplica si son pedidos de woocomerce",
        copy=False
    )
    woo_receta_html = fields.Html(
        string="Receta",
        help="Receta de",
        copy=False
    )
    woo_receta = fields.Char(
        string="Receta",
        help="Receta de",
        copy=False
    )
    commitment_date_one_delivery = fields.Datetime('Fecha promesa un solo envío', copy=False )
    body_meta = fields.Char(string="Body json", copy=False)
    one_delivery_date = fields.Boolean(string="Un solo envío", copy=False)
    invoice_discount_rate = fields.Float(
        'Descuento',
        readonly=True,
        store=True,
        copy=False,
        states={
            'draft': [('readonly', False)]
        })

class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    fecha_promesa = fields.Datetime(string="Fecha promesa", compute="_compute_fecha_promesa", store=True, copy=False)
    is_split = fields.Boolean(string="Se realizo split", store=True, copy=False)

    @api.depends("line_warehouse_id", "order_id.one_delivery_date")
    def _compute_fecha_promesa(self):
        for record in self:
            if record.order_id.body_meta:
                record.fecha_promesa = record.obtener_fecha_promesa(record.order_id.body_meta)

    def get_products_qty(self):
        products = {}
        for line in self.order_id.order_line:
            if products.get(line.product_id.id):
                products[line.product_id.id] += line.product_uom_qty
            else:
                products[line.product_id.id] = line.product_uom_qty
        return products

    def obtener_fecha_promesa(self, body_meta):
        fecha_promesa = False
        if self.product_id.channel_mapping_ids:
            if self.order_id.one_delivery_date:
                return self.order_id.commitment_date_one_delivery
            products_qty = self.get_products_qty()
            woo_store_id = self.product_id.channel_mapping_ids[0].store_product_id
            delivery_date_key = f'delivery_dates_{woo_store_id}'
            list_meta = json.loads(body_meta)
            for meta in list_meta.get("meta_data"):
                if meta["key"] == delivery_date_key:
                    delivery_dates = meta["value"]["delivery_dates"]
                    if self.line_warehouse_id.woo_branch:
                        if self.is_split:
                            product_qty = products_qty.get(self.product_id.id)
                        else:
                            product_qty = self.product_qty
                        ava_qty = self.env['stock.quant']._get_available_quantity(
                            self.product_id,
                            self.line_warehouse_id.view_location_id, None, strict=False) - product_qty
                        if ava_qty < 0 and "_stock_at_4196" in delivery_dates and delivery_dates["_stock_at_4196"]:
                            date_promesa = delivery_dates["_stock_at_4196"]
                            fecha_promesa = (
                                    datetime.fromisoformat(date_promesa)+timedelta(hours=5)) if date_promesa else False
                            break
                        if self.line_warehouse_id.woo_branch in delivery_dates and delivery_dates[self.line_warehouse_id.woo_branch]:
                            date_promesa = delivery_dates[self.line_warehouse_id.woo_branch]
                            fecha_promesa = (datetime.fromisoformat(date_promesa) + timedelta(hours=5)) if date_promesa else False
                            break
                        if "_stock_at_4196" in delivery_dates and delivery_dates["_stock_at_4196"]:
                            date_promesa = delivery_dates["_stock_at_4196"]
                            fecha_promesa = (datetime.fromisoformat(date_promesa) + timedelta(hours=5)) if date_promesa else False
                            break
                        keys = self.env["stock.warehouse"].search([]).filtered(lambda x: x.woo_branch).mapped(
                            "woo_branch")
                        for k in keys:
                            if delivery_dates.get(k):
                                date_promesa = delivery_dates[k]
                                fecha_promesa = (datetime.fromisoformat(date_promesa) + timedelta(
                                    hours=5)) if date_promesa else False
                                break
        if not fecha_promesa:
            fecha_promesa = self.get_fecha_promesa()
        return fecha_promesa

    def get_fecha_promesa(self, cp=False, product_store_id=False, qty=False):
        fecha_promesa = False
        if self.order_id.partner_shipping_id.zip:
            codigo_zip = self.order_id.partner_shipping_id.zip
            for store_wooo in self.product_id.channel_mapping_ids:
                template_id = store_wooo.store_product_id
                channel = store_wooo.channel_id
                url = f'{channel.woocommerce_url}/wp-json/prixz-pronto/fecha_promesa_producto/{codigo_zip}/{template_id}/{int(self.product_uom_qty)}'
                response = requests.get(url)
                if not response.ok:
                    _logger.info(f"Error al obtener fecha promesa{response.text}")
                else:
                    json_response = json.loads(response.text)
                    delivery_dates = json_response.get("delivery_dates")
                    if delivery_dates.get(self.line_warehouse_id.woo_branch):
                        date_promesa = delivery_dates.get(self.line_warehouse_id.woo_branch)
                        if date_promesa:
                            fecha_promesa = datetime.fromisoformat(date_promesa) + timedelta(hours=5)
                    elif delivery_dates.get("fecha_entrega_crum_virtual"):
                        date_promesa = delivery_dates.get("fecha_entrega_crum_virtual")
                        if date_promesa:
                            fecha_promesa = datetime.fromisoformat(date_promesa) + timedelta(hours=5)
        return fecha_promesa

    def _prepare_procurement_values(self, group_id=False):
        values = super(SaleOrderLine, self)._prepare_procurement_values(group_id)
        self.ensure_one()
        if self.order_id.order_state_woo:
            if self.fecha_promesa:
                values['date_deadline'] = self.fecha_promesa
        return values


class OrderFeed(models.Model):
    _inherit = 'order.feed'

    woo_date_paid = fields.Datetime(
        string="Fecha de pago en woo",
        help="Fecha de pago realizado en woo solo aplica si son pedidos de woocomerce"
    )
    woo_receta = fields.Char(
        string="Receta",
        help="Receta de"
    )
    woo_receta_html = fields.Html(
        string="Receta",
        help="Receta de"
    )
    body_meta = fields.Char(string="fecha promesa")
    one_delivery_date = fields.Boolean(string="Un solo envío")
    commitment_date = fields.Datetime('Fecha promesa un solo envío', copy=False )
    invoice_discount_rate = fields.Float(
        'Descuento',
        readonly=True,
        store=True,
        states={
            'draft': [('readonly', False)]
        })