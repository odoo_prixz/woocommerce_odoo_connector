from odoo import fields, models
from odoo.addons.odoo_multi_channel_sale.tools import parse_float,extract_list as EL

from logging import getLogger
_logger = getLogger(__name__)

""" 28/01/2022 Cesar Equihua: Agregar el status del feed de woo en el pedido """
class InheritSaleOrder(models.Model):
    _inherit = 'sale.order'

    order_state_woo = fields.Char(string="Estado Woo", copy=False)


class InheritOrderFeed(models.Model):
    _inherit="order.feed"

    def import_order(self,channel_id):
        if channel_id.channel != "woocommerce":
            return super().import_order(channel_id)
        else:
            message = ""
            update_id=None
            create_id=None
            self.ensure_one()
            vals = EL(self.read(self.get_order_fields()))
            store_id = vals.pop('store_id')
            if self.woo_date_paid:
                vals['woo_date_paid'] = self.woo_date_paid
            if self.woo_receta:
                vals['woo_receta'] = self.woo_receta
            if self.woo_receta_html:
                vals['woo_receta_html'] = self.woo_receta_html
            if self.invoice_discount_rate:
                vals['invoice_discount_rate'] = self.invoice_discount_rate
            if self.one_delivery_date:
                vals['one_delivery_date'] = self.one_delivery_date
            if self.one_delivery_date:
                vals['commitment_date_one_delivery'] = self.commitment_date
            if self.body_meta:
                vals['body_meta'] = self.body_meta
            store_source = vals.pop('store_source')
            match = self._context.get('order_mappings').get(channel_id.id,{}).get(store_id)
            if match:
                match = self.env['channel.order.mappings'].browse(match)
            state = 'done'
            store_partner_id = vals.pop('partner_id')


            date_info = self.get_order_date_info(channel_id,vals)
            if date_info.get('confirmation_date'):
                vals['date_order']=date_info.get('confirmation_date')
            elif date_info.get('date_order'):
                vals['date_order']=date_info.get('date_order')

            date_invoice =  date_info.get('date_invoice')
            confirmation_date = date_info.get('confirmation_date')

            if store_partner_id and self.customer_name:
                if not match:
                    res_partner = self.get_order_partner_id(store_partner_id,channel_id)
                    message += res_partner.get('message','')
                    partner_id = res_partner.get('partner_id')
                    partner_invoice_id = res_partner.get('partner_invoice_id')
                    partner_shipping_id = res_partner.get('partner_shipping_id')
                    if partner_id and partner_invoice_id and partner_shipping_id:
                        vals['partner_id'] = partner_id.id
                        vals['partner_invoice_id'] = partner_invoice_id.id
                        vals['partner_shipping_id'] = partner_shipping_id.id
                        # Se forza a tener los campos del envio en el partner_id **Richar**
                        partner_id.street = partner_shipping_id.street
                        partner_id.street2 = partner_shipping_id.street2
                        partner_id.zip = partner_shipping_id.zip
                        partner_id.city = partner_shipping_id.city
                        ##########FIN
                    else:
                        message += '<br/>Partner,Invoice,Shipping Address must present.'
                        state = 'error'
                        _logger.error('#OrderError1 %r'%message)
                #se forza a actualizar la info del partner
                else:
                    try:
                        if match.odoo_partner_id:
                            match.odoo_partner_id.street = self.shipping_street
                            match.odoo_partner_id.street2 = self.shipping_street2
                            match.odoo_partner_id.zip = self.shipping_zip
                            match.odoo_partner_id.city = self.shipping_city
                            if not store_partner_id.isnumeric():
                                match.odoo_partner_id.email = str(store_partner_id)
                    except Exception as e:
                            _logger.error('El partner no se encuentra %s %s' % store_partner_id, str(e))
                 #se forza a actualizar la info del partner
            else:
                message += '<br/>No partner in sale order data.'
                state = 'error'
                _logger.error('#OrderError2 %r'%message)


            if state=='done':
                carrier_id = vals.pop('carrier_id', '')

                if carrier_id:
                    carrier_res = self.get_carrier_id(carrier_id, channel_id=channel_id)
                    message += carrier_res.get('message')
                    carrier_id = carrier_res.get('carrier_id')
                    if carrier_id:
                        vals['carrier_id'] = carrier_id.id
                order_line_res = self._get_order_line_vals(vals, carrier_id, channel_id)
                message += order_line_res.get('message','')
                if not order_line_res.get('status'):
                    state = 'error'
                    _logger.error('#OrderError3 %r'%order_line_res)
                else:
                    order_line = order_line_res.get('order_line')
                    if len(order_line):
                        vals['order_line'] = order_line
                        state = 'done'
            currency=self.currency

            if state=='done' and currency:
                currency_id = channel_id.get_currency_id(currency)
                if not currency_id:
                    message += '<br/> Currency %s no active in Odoo'%(currency)
                    state = 'error'
                    _logger.error('#OrderError4 %r'%message)
                else:
                    pricelist_id = channel_id.match_create_pricelist_id(currency_id)
                    vals['pricelist_id']=pricelist_id.id

            vals.pop('id')
            vals.pop('website_message_ids','')
            vals.pop('message_follower_ids','')
            vals['team_id'] = channel_id.crm_team_id.id
            vals['warehouse_id'] = channel_id.warehouse_id.id


            if match and match.order_name:
                if  state =='done' :
                    try:
                        order_state = vals.pop('order_state')
                        """ 28/01/2022 Cesar Equihua: Actulizar el estado del pedido"""
                        vals['order_state_woo'] = order_state
                        if match.order_name.state=='draft':
                            match.order_name.write(dict(order_line=[(5,0)]))
                            match.order_name.write(vals)
                            message +='<br/> Order %s successfully updated'%(vals.get('name',''))
                        else:
                            message+='Only order state can be update as order not in draft state.'
                        message += self.env['multi.channel.skeleton']._SetOdooOrderState(match.order_name,channel_id,
                                order_state,self.payment_method,date_invoice=date_invoice,confirmation_date=confirmation_date)
                    except Exception as e:
                        message += '<br/>%s' % (e)
                        _logger.error('#OrderError5  %r'%message)
                        state = 'error'
                    update_id = match
                elif state =='error':
                    message+='<br/>Error while order update.'
            else:
                if state == 'done':
                    try:
                        order_state = vals.pop('order_state')
                        """ 28/01/2022 Cesar Equihua: Registrar el estado del pedido"""
                        vals['order_state_woo'] = order_state
                        erp_id = self.env['sale.order'].create(vals)
                        """ 21/01/2022 Cesar Equihua: Cancelar la orden si el monto es menor a 0 """
                        
                        if erp_id.amount_total < 0:
                            erp_id.action_cancel()
                        message += self.env['multi.channel.skeleton']._SetOdooOrderState(erp_id,channel_id,order_state,self.payment_method,date_invoice=date_invoice,confirmation_date=confirmation_date)
                        message  += '<br/> Order %s successfully evaluated'%(self.store_id)
                        create_id =  channel_id.create_order_mapping(erp_id,store_id,store_source)

                    except Exception as e:
                        message += '<br/>%s' % (e)
                        _logger.error('#OrderError6 %r'%message)
                        state = 'error'
            self.set_feed_state(state=state)
            self.message = "%s <br/> %s" % (self.message,message)
            return dict(
                create_id=create_id,
                update_id=update_id,
                message=message
            )
            

