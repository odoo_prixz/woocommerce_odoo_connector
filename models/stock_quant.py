from odoo import models, _
from woocommerce import API
import logging
import time
import threading


_logger = logging.getLogger(__name__)


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    def _prepare_datas_update_stock(self, stocks_ids):
        products = []
        no_update = []
        all_products = []
        contador = 0
        for record in stocks_ids:
            product_map_id = self.env["channel.product.mappings"].search([
                ("product_name", "=", record.product_id.id)
            ])
            if product_map_id:
                for map in product_map_id:
                    data = {
                        "id": map.store_product_id,
                        "stock_quantity": int(record.available_quantity),
                        "product_id": map.product_name.id
                    }
                    products.append(data)
                    contador = contador + 1
                    if contador == 20:
                        contador = 0
                        all_products.append(products)
                        products = []
            else:
                no_update.append(record.product_id.barcode)
        if contador < 20:
            all_products.append(products)
        return all_products, no_update

    def proceso_enviar(self, connection, url, all_products):
        _logger.warning("********Inicio proceso %s *************", str(threading.current_thread().getName()))
        contador = 0
        for lines in all_products:
            data = {
                "updated": lines,
            }
            inicio = time.time()
            request_post = connection.post(
                url,
                data
            )
            fin = time.time()
            tiempo_final = fin - inicio
            _logger.warning("********Proceso %s tiempo : %s linea: %s", str(threading.current_thread().getName()), str(tiempo_final), str(contador))
            contador = contador + 1
            if not request_post.ok:
                _logger.warning("lote no se actualizo correcto: %s \n %s", str(lines), str(request_post.text))
            else:
                _logger.warning("lote correcto: %s \n %s", str(lines), str(request_post.text))

    def enviar_por_producto(self, connection, url, all_products):
        for lines in all_products:
            for line in lines:
                stock_id = self.search([
                    ("location_id.name", "=", "Stock"),
                    ("location_id.location_id.name", "=", "CRUMS"),
                    ("product_id", "=", line.get("product_id"))
                ], limit=1)
                if stock_id:
                    data = {
                        "stock_quantity": int(stock_id.available_quantity),
                    }
                else:
                    data = {
                        "stock_quantity": line.get("stock_quantity"),
                    }
                URL_PATH = url + line.get("id")
                request_post = connection.post(
                    URL_PATH,
                    data
                )
                if not request_post.ok and request_post.request.body:
                    _logger.warning("****Proucto no se actualizo correcto: %s \n %s****", str(lines),
                                    str(request_post.request.body))
                else:
                    _logger.warning("Producto correcto: %s \n %s", str(lines), str(request_post.request.body))

    def _cron_update_stock_bernabe_woo(self):
        url = "products/"
        stocks_ids = self.search([
            ("location_id.name", "=", "Stock"),
            ("location_id.location_id.name", "=", "CRUMS")
        ], order='id desc')
        connection = self._get_woocommerce_connection()
        if connection:
            all_products, no_update = self._prepare_datas_update_stock(stocks_ids)
            self.enviar_por_producto(connection, url, all_products)
            if no_update:
                _logger.warning("Stock no actualizado, no se encuentran los registros en map: %s", str(no_update))

    def _get_woocommerce_connection(self):
        channel_id = self.env['multi.channel.sale'].sudo().search([
            ("channel", "=", "woocommerce"),
            ("state", "=", "validate")
        ], limit=1)
        if channel_id:
            req = API(
                url=channel_id.woocommerce_url,
                consumer_key=channel_id.woocommerce_consumer_key,
                consumer_secret=channel_id.woocommerce_secret_key,
                #wp_api=True,
                version="wc/v3",
                #query_string_auth=True,
                timeout=999,
            )
            return req
        else:
            _logger.info("No connection channel Woo available")
            return False
